"""Main Aldani package."""
from typing import List, AnyStr, Tuple
from subprocess import Popen, PIPE
import logging
import hashlib
import os
import mimetypes

import requests

from virustotal import Client


def write_report(results: List[AnyStr], report_filename: str):
    """Write a list of results to a given report filename."""
    with open(report_filename, 'w') as report:
        for result in results:
            report.write(f'{result}\n')


def strings(filename: str) -> Tuple[bytes, bytes]:
    """Pass the given ``filename`` to the Unix ``strings`` binary and return the results."""
    logging.info(f'Running unix strings command on {filename}')
    process = Popen(['strings', filename], stdout=PIPE, stderr=PIPE)
    stdout, stderr = process.communicate()
    if stderr:
        logging.error(f'Error while running strings: {stderr}')

    return stdout, stderr


def get_sha256(filename: str) -> str:
    """Get the SHA256 hash of a given file."""
    logging.info(f'Attempting to get SHA256 hash of {filename}')
    blocksize = 65536
    hasher = hashlib.sha256()
    with open(filename, 'rb') as file:
        buf = file.read(blocksize)
        while len(buf) > 0:
            hasher.update(buf)
            buf = file.read(blocksize)
            sha256 = hasher.hexdigest()

    return sha256


def get_remote_file(url: str, filename: str) -> str:
    """Do a GET request to the given ``url`` and try to write the response to ``filename``."""
    logging.info(f'Downloading {filename} from {url}')
    response = requests.get(url=url, allow_redirects=True)
    response.raise_for_status()
    with open(filename, 'wb') as file:
        file.write(response.content)

    return filename


def upload_and_scan_file(vt_client: Client, filename: str, url: str = None) -> List[AnyStr]:
    """Upload a scan a file using the VirusTotal API.

    If a filename is given but no URL, assume the file is on the local machine.

    If a URL is provided, download the file first then save it as the given filename.
    """
    # If a URL was given, download the file and name it ``filename``
    if url:
        get_remote_file(url=url, filename=filename)

    # Split the filename from the extension
    f_name, f_extension = os.path.splitext(filename)

    # Determine file mime type
    mime_type = mimetypes.guess_type(filename)
    if mime_type is None:
        logging.error('Cannot guess file type!')
        return []

    # Determine file extension if os.path.splitext didn't get one from the filename
    if not f_extension:
        f_extension = mimetypes.guess_extension(mime_type[0])

    # If mimetypes couldn't guess the extension then return
    if not f_extension:
        logging.error('Cannot guess file extension and no extension on input file')
        return []

    fextension = f'File extension: {f_extension}'
    fmime = f'File MIME type: {mime_type[0]}'

    # Run SHA256 Hash against file
    sha256 = f'SHA256 Hash: {get_sha256(filename=filename)}'

    # Upload to VirusTotal
    vt_response = vt_client.scan_file(filename=filename)

    # Run strings on the file
    strings_stdout, strings_stderr = strings(filename=filename)

    return [filename, url, fextension, fmime, sha256, vt_response, strings_stdout]
