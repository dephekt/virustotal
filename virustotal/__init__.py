"""Package implementing VirusTotal functionality."""
from typing import Dict, Any, Union, Optional, Tuple
import logging

from urllib3.util.retry import Retry

from requests.adapters import HTTPAdapter
import requests

logging.basicConfig(level=logging.DEBUG)

RETRY_STRATEGY = Retry(
    # Default is 10
    total=2,
    # VirusTotal API uses an absurd 204 to indicate
    # rate limit exceeded which is not retried by
    # default in Requests.
    status_forcelist=[204, 429, 500, 502, 503, 504],
    # POST is excluded by default because POST could
    # result in a new insert, however, most APIs will
    # (sanely) not return an error code AND perform an
    # insert in the same call.
    method_whitelist=["HEAD", "GET", "PUT", "POST", "DELETE", "OPTIONS", "TRACE"],
)

DEFAULT_TIMEOUT = (6, 27)


class TimeoutHttpAdapter(HTTPAdapter):
    """Request adapter enforcing a default timeout on all requests."""

    def __init__(self, *args, **kwargs):
        self.timeout = DEFAULT_TIMEOUT

        if "timeout" in kwargs:
            self.timeout = kwargs["timeout"]
            del kwargs["timeout"]

        super().__init__(*args, **kwargs)

    def send(self, request: requests.PreparedRequest, **kwargs) -> requests.Response:
        """Send a given :class:`PreparedRequest`."""
        timeout = kwargs.get("timeout")

        if timeout is None:
            kwargs["timeout"] = self.timeout

        return super().send(request=request, **kwargs)


class VirusTotal:
    """A client implementing the VirusTotal v2 API.

    Some methods implement endpoints available only to Private API
    users. If you require use of those methods, set ``use_private=True``
    when creating your instance of this class.

    A :class:`RuntimeException` is raised if a private endpoint method
    is called without having specified ``use_private=True``.

    Provide your own HTTP transport adapter, session, or timeout to
    override default behaviors.
    """

    def __init__(
        self,
        api_key: str,
        api_url: str = "https://www.virustotal.com/vtapi/v2",
        use_private: bool = False,
        timeout: Union[None, float, Tuple[float, float], Tuple[float, None]] = DEFAULT_TIMEOUT,
        session: Optional[requests.Session] = None,
        adapter: Optional[HTTPAdapter] = None,
    ):
        self.api_url = api_url
        self.use_private = use_private
        self.timeout = timeout

        # v2 API is absurd and requires the API key be specified
        # as a query parameter rather than via a header
        self.request_params = {"apikey": api_key}

        if adapter:
            self.adapter = adapter
        else:
            self.adapter = TimeoutHttpAdapter(max_retries=RETRY_STRATEGY, timeout=self.timeout)

        if session:
            self.session = session
        else:
            self.session = requests.Session()
            self.session.hooks["response"] = [self._assert_status_hook, self._debug_log_headers]
            if self.adapter:
                self.session.mount(prefix="https://", adapter=self.adapter)
                self.session.mount(prefix="http://", adapter=self.adapter)

    @staticmethod
    def _assert_status_hook(response: requests.Response, *args, **kwargs):
        response.raise_for_status()

    @staticmethod
    def _debug_log_headers(response: requests.Response, *args, **kwargs):
        logging.debug("Response Headers: %s", response.headers)

    def scan_file(self, filename, upload_url: str = None) -> Dict[str, Union[str, int]]:
        """Allows sending a file for scanning with VirusTotal.

        File size limit is 32MB, in order to submit files up to 200MB in size
        you must request a special upload URL using Client.get_scan_upload_url method.

        https://developers.virustotal.com/reference#file-scan
        """
        url = f"{self.api_url}/file/scan"

        # If we got a special upload URL, use that instead of the usual endpoint
        if upload_url:
            url = upload_url
            # Special URL includes API key already, so we don't pass that in this case
            self.request_params = {}

        with open(filename, "rb") as file:
            files = {"file": (filename, file)}

            return self.session.post(url=url, files=files, params=self.request_params).json()

    def rescan_file(self, resource: str) -> Dict[str, Union[int, str]]:
        """Re-scan a file.

        This endpoint is available in the Private API only.

        This API has the potential to produce a denial of service on the
        scanning infrastructure if abused. Please contact VirusTotal if
        you are going to be rescanning more than 50,000 files per day.

        The ``resource`` argument can be the MD5, SHA-1, or SHA-256 of
        the file you want to re-scan.
        """
        if not self.use_private:
            raise RuntimeError("Use of private API endpoints is not allowed.")

        url = f"{self.api_url}/file/rescan"

        self.request_params["resource"] = resource

        return self.session.post(url=url, params=self.request_params).json()

    def get_scan_upload_url(self) -> Dict[str, str]:
        """Get a special URL for uploading files larger than 32MB.

        This API requires additional privileges. You must contact VirusTotal
        support to request special authorization to use this endpoint.

        https://developers.virustotal.com/reference#file-scan-upload-url
        """
        if not self.use_private:
            raise RuntimeError("Use of private API endpoints is not allowed.")

        url = f"{self.api_url}/file/scan/upload_url"

        return self.session.get(url=url, params=self.request_params).json()

    def download_file(self, file_hash: str, filename: Optional[str]):
        """Download a file.

        This endpoint is available in the Private API only.

        The ``file_hash`` argument should be the MD5, SHA-1 or SHA-256 hash
        of the file you want to download.

        The ``filename`` argument is the filename to store the downloaded file.
        If none is given, the value of file_hash will be used.
        """
        if not self.use_private:
            raise RuntimeError("Use of private API endpoints is not allowed.")

        url = f"{self.api_url}/file/download"

        self.request_params["hash"] = file_hash

        response = self.session.get(url=url, params=self.request_params)

        if not filename:
            filename = file_hash

        with open(filename, "wb") as download:
            download.write(response.content)

    def get_file_report(self, resource: str, allinfo: bool = False) -> Dict[str, Any]:
        """Retrieve file scan reports.

        The ``resource`` argument can be the MD5, SHA-1 or SHA-256 of a file
        for which you want to retrieve the most recent antivirus report. You
        may also specify a scan_id returned by the ``Client.scan_file`` method.

        If the ``allinfo`` argument is ``True`` additional info other than the
        antivirus scan results is returned. See docs for more info.

        The ``allinfo`` argument is available in the Private API only.

        https://developers.virustotal.com/reference#file-report
        """
        if not self.use_private and allinfo:
            raise RuntimeError("Use of private API endpoints is not allowed.")

        url = f"{self.api_url}/file/report"

        self.request_params["resource"] = resource
        self.request_params["allinfo"] = allinfo

        return self.session.get(url=url, params=self.request_params).json()
