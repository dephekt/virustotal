# VirusTotal

A Python client for implementing the [VirusTotal v2 API](https://developers.virustotal.com/reference#getting-started).

## Usage

```python
from virustotal import VirusTotal

vt = VirusTotal("your_api_key")

results = vt.scan_file("myfile.txt")
report = vt.get_file_report(results["scan_id"])
```

If you need to use [private API](https://developers.virustotal.com/reference#public-vs-private-api) endpoints, pass `use_private=True` when making your client:

```python
from virustotal import VirusTotal

vt = VirusTotal("your_api_key", use_private=True)

upload_url = vt.get_scan_upload_url()["upload_url"]

results = vt.scan_file("my_big_file.exe", upload_url=upload_url)
print(f"Scan ID: {results['scan_id']}\nPermalink: {results['permalink']}")
```

This will not magically grant you access to the private API. You need to contact VirusTotal to sign-up for premium services. This simply prevents the client from raising a `RuntimeError` when you try calling a restricted method.