#!/usr/bin/env python
import os
import logging
import sys

import typer

from virustotal import VirusTotal as Client

import aldani

logging.basicConfig(level=logging.DEBUG)

app = typer.Typer()


@app.command()
def scan_file(filename: str, report_name: str, api_key: str = None):
    if api_key:
        vt = Client(api_key=api_key)
    else:
        try:
            vt = Client(api_key=os.environ['VT_API_KEY'])
        except KeyError:
            logging.error('An API key is required, export it to VT_API_KEY or pass it as an arg using --api-key')
            sys.exit(1)

    logging.info(f'Preparing to upload and scan {filename}')
    vt_results = aldani.upload_and_scan_file(vt_client=vt, filename=filename)

    if vt_results:
        logging.info(f'Writing report to {report_name}')
        aldani.write_report(results=vt_results, report_filename=report_name)
    else:
        logging.error(f'No scan results to write! Was this expected?')
        sys.exit(1)


@app.command()
def sha256(filename: str):
    file_hash = aldani.get_sha256(filename=filename)
    print(f'SHA 256 Hash: {file_hash}')


@app.command()
def download_file(url: str, filename: str):
    aldani.get_remote_file(url=url, filename=filename)
    print(f'Downloaded file to {filename}')


if __name__ == '__main__':
    app()
